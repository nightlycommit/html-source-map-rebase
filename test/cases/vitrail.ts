import {TwingEnvironment, TwingLoaderFilesystem, TwingLoaderRelativeFilesystem} from "twing";
import {resolve} from "path";
import tape from "tape";
import {readFileSync} from "fs";
import {createRebaser} from "../../src/lib/Rebaser";

const warmUp = function () {
    let loader = new TwingLoaderRelativeFilesystem();

    return new TwingEnvironment(loader, {
        source_map: true
    });
};

tape('Vitrail', ({test}) => {
    test('should handle well-formed map', ({same, end}) => {
        const environment = warmUp();

        return environment.render('../fixtures/vitrail/styleguide/logo/index.html.twig').then((html) => {
            const map = environment.getSourceMap();

            let rebaser = createRebaser(Buffer.from(map));

            return rebaser.rebase(Buffer.from(html))
                .then(({data}) => {
                    const expectation = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
        <link rel="stylesheet" href="../fixtures/inclusion-from-block/styleguide/logo/index.scss"/>
</head>
<body>
    <img src="../fixtures/inclusion-from-block/src/components/logo/assets/vitrail.png" alt="Vitrail Logo"/>
</body>
</html>`;

                    same(data.toString(), expectation.toString());

                    end();
                });
        });
    });
});